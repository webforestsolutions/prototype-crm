  import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgDatepickerModule } from 'ng2-datepicker';
import { Deal } from './shared/deal';
import { DealService } from './shared/deal.service';

import { Stage } from './shared/stage';
import { StageService } from './shared/stage.service';

import { Details } from './shared/details';
import { Task } from './shared/task';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { DragulaService, DragulaModule } from 'ng2-dragula/ng2-dragula';

declare var $:any;
declare var swal:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {


    enableCreateBox: boolean = false;
    enableDescriptionBox: boolean = false;

    deal: Deal = new Deal();
    details: Details = new Details();
    deals: any;
    dealCollection: Array<any> = [];

    stage: Stage = new Stage();
    stages: any;
    enableUpdate: boolean = false;

    position: any;
    comments: Array<any> = [];
    location = {};
    currLocation: any;
    currDate: any;
    hv = window.innerHeight;
    hw = window.innerWidth;
    readyStatus: boolean = false;

    currentDeal: any;
    currentDealStatus: Array<any> = [];

    detailsCollection: Array<any> = [];
    enableAddDetails: boolean = false;
    enableNote: boolean = false;
    enableTask: boolean = false;

    activities: Array<any> = [];
    note: any;
    task: task = new Task();

     editorOptions = {
          placeholder: "Insert note...",
          modules: {
            toolbar: [
              ['bold', 'italic', 'underline', 'strike'],
              [{ 'list': 'ordered'}, { 'list': 'bullet' }]
            ]
          }
        }; 


    constructor(private ds: DealService, private ss: StageService, private dragulaService: DragulaService){ 
        dragulaService.drop.subscribe((value) => {
            let [e, el, container] = value.slice(1);
            this.getLocation(); 
            this.updateStageDeal(e.id, e.parentNode.id);

        });
        this.task.date = Date.now();
        this.task.time = new Date().toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");


    }

    ngOnInit(){
        this.stages = this.sanitizeStages(this.ss.getStagesList());     
        this.getCoordinates();
        this.currDate = this.formatDate(new Date());

    }

    createStage(): void{

    	if(this.stages.length > 0){
            this.stage.index = this.stages[this.stages.length - 1].index + 1;
        } else{
            this.stage.index = 0;
        }
    	this.ss.addStage(this.stage);
    	$('#modalForm').modal('hide');

    	this.success(this.stage.name + ' was successfully added!');
    	this.stage =  new Stage();
    	// setTimeout(function() { location.reload(); }, 2000);
    }
    
    editStage(stage): void{
    	this.stage = stage;
    	$('#modalForm').modal('show');
    	this.enableUpdate = true;
    }	

    updateStage(): void{
        this.stage.deals = this.sanitizeDealsData(this.stage.deals);
    	this.ss.updateStage(this.stage);
    	this.success(this.stage.name + ' stage was successfully updated!');
    	$('#modalForm').modal('hide');
    }

    deleteStage(): void{
    	this.ss.deleteStage(this.stage);
    	$('#modalForm').modal('hide');
    }


    createDeal(): void{
        for(var i = 0; i <= this.deal.stage; i++){
            this.currentDealStatus.push({
                'date': this.currDate,
                'location': this.currLocation,
                'stage': i
            });
        }
        var time = new Date().toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
        this.deal.created = this.currDate + ' ' + time;
        this.deal.status = this.currentDealStatus;
        this.deal.date = this.formatDate(this.deal.date);
    	this.ss.addDeal(this.deal);
    	this.success('Deal was successfully added!');
    	this.deal =  new Deal();
        this.currentDealStatus = [];
    	$('#modalDealForm').modal('hide');
    }

    updateStageDeal(dealId, stageId): void{
        this.ss.updateStageDeal(dealId.split('-')[1], stageId, dealId.split('-')[0], this.currLocation, this.currDate);
    }

    addDetails(deal): void{
        if(typeof(deal.details) !== 'undefined'){
            this.detailsCollection = deal.details;
        } else {
             this.detailsCollection = [];
        }
        this.detailsCollection.push({'label': this.details.label, 'value': this.details.value);
       
        deal.details = this.detailsCollection;
        this.ss.updateDeal(deal);
        this.enableAddDetails = false;
        this.details = new Details();
    }

    addNote(deal): void{

        if(typeof(deal.activities) !== 'undefined'){
            this.activities = deal.activities;
        } else {
             this.activities = [];
        }
        var time = new Date().toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
        this.activities.push({'type': 'note', 'value': this.note , 'created' :   this.currDate + ' ' + time});
        deal.activities = this.activities;
        this.ss.updateDeal(deal);
        this.note = '';
        this.enableNote = false;
    }

    addTask(deal): void{
      if(typeof(deal.activities) !== 'undefined'){
        this.activities = deal.activities;
      } else {
        this.activities = [];
      }
      var time = new Date().toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
      var task = {
        'type': 'task',
        'title': this.task.title,
        'details': this.task.details,
        'date': this.formatDate(this.task.date),
        'time':  this.task.time, 
        'created' :   this.currDate + ' ' + time
      }


       this.activities.push(task);
       //   console.log(this.activities);
      deal.activities = this.activities;
      this.ss.updateDeal(deal);
      this.task = new Task();
      this.task.date = Date.now();
      this.enableTask = false;
    }



   	sanitizeStages(data){
	    data.forEach(item =>{
	        this.stages = this.sanitizeDataWithIndex(item);
	    });
	}

	sanitizeDealsData(data){
	    var dataContainer = [];
	    $.map(data, function(value, index) {
	        dataContainer[index] = value;
	    });
	    return dataContainer;
	 }


	sanitizeDataWithIndex(data){
	    var dataContainer = [];
	    $.map(data, function(value, index) {
	        dataContainer[index] = value;
	    });
	    return dataContainer;
	 }

     toArray(): void{
         for (var i = 0; i < this.stages.length; i++) {
             this.stages[i].deals = this.sanitizeDataWithIndex(this.stages[i].deals);
             console.log(this.stages[i].deals);
         }
     }

	 cancelCard(): void{
	 	this.deal = new Deal();
	 	this.enableCreateBox = false;
	 }

	 cardMove(): void{
	 	console.log('Moved');
	 	//this.geocodeLatLng();
	 }

	 openModal(deal): void{

	 	this.currentDeal = deal;
	 	window["$"]('#showDeal').modal('show');
	 }

	 updateDeal(currentDeal): void{
	 	this.enableDescriptionBox = false;
	 }

	 comment(){
	 	this.comments.push($('#comment').val());
	 	this.currentDeal.comments = this.comments;
	 }

	 newStage(){
	 	this.stage =  new Stage();
	 	this.enableUpdate = false;
	 	$('#modalForm').modal('show');
	 }

	 newDeal(){
        this.getLocation(); 
	 	this.deal =  new Deal();
	 	this.deal.date = Date.now();
	 	this.deal.stage = 0;
	 	this.enableUpdate = false;
	 	$('#modalDealForm').modal('show');
	 }

     viewDeal(deal):void{
        $('#deal').modal('show');
        this.currentDeal = deal;
     }


	 success(message): void{
	 	swal({
		  icon: "success",
		  title: message
		});

	 }

	onResize(event){
 		this.hw = window.innerWidth;
 		this.hv = window.innerHeight;
 	}

       private onDrop(args) {
        let [el, target, source] = args;
      }
	
    getCoordinates(){
        if (navigator.geolocation) {
           navigator.geolocation.getCurrentPosition(this.showPosition.bind(this));
        } else {
             console.log("Geolocation is not supported by this browser.");
        }
    }

     showPosition(position){
        this.location = position.coords;
        console.log(this.location);
        this.readyStatus = true;
        
    }


    getLocation() {

      var place = new google.maps.LatLng(this.location.latitude,this.location.longitude);

      var map = new google.maps.Map(document.getElementById('map'), {
          center: place,
          zoom: 15
        });

      var request = {
        location: place,
        radius: '20'
      };

      var service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, this.callback.bind(this));
    }

    callback(results, status) {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        this.currLocation = results[1].vicinity;
      }
    }

    formatDate(date) {
      var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
      ];

      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();
      var time = new Date().toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");

      return monthNames[monthIndex] + ' ' + day + ', ' + year + ' ' + time;
    }


}