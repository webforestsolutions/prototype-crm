export class Deal {
	id: string;
	title: string;
	description: string;
	contact: any;
	organization: string;
	value: string;
	stage: any;
	date: any;
	status: any;
	created: any;
	timestamp: any;
	location: any;
	details: any;
	activities: any;
}