import { Injectable, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import {Stage} from './stage';
import {Deal} from './deal';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


declare var $:any;

@Injectable()




export class StageService{

	stagesCollection: AngularFirestoreCollection<Stage>;
	stages: Observable<Stage[]>;
	stage: Observable<Stage>;
	deals: Observable<Deal[]>;
	deal: Observable<Deal>;

	constructor(private afs: AngularFirestore){
		this.stagesCollection = this.afs.collection('columns', ref => ref.orderBy('index') );
	}

	getStagesList(){
		this.stages = this.stagesCollection.valueChanges()
			.map(items => {
		    for (let item of items) {
		      item.deals = this.stagesCollection.doc('stage-'+item.index).collection("deals").valueChanges();
		    }
		    return items;
		  });
		return this.stages;
	}

	addStage(stage: Stage): void  {
	  const id = this.afs.createId();

	  this.stagesCollection.doc('stage-'+ stage.index).set(Object.assign({}, stage));
	}

	updateStage(stage: Stage): void  {

	  this.stagesCollection.doc('stage-'+ stage.index).update(stage);
	}


	updateStageDeal(dealId, newStageId, oldStageId, currLoc, currDate):void {
		this.stagesCollection.doc('stage-'+oldStageId + '/deals/'+dealId).valueChanges().subscribe(value => {
		  let deal = value;
		  let status = [];
		  deal.stage = parseInt(newStageId);
		  for(var i = 0; i <= parseInt(newStageId); i++){
			  if(i == parseInt(newStageId)){
			  	deal.status[parseInt(newStageId)] = {
				  	'date': currDate,
				  	'location': currLoc,
				  	'stage': parseInt(newStageId)
				 } 
			  } else{
			  	if(typeof(deal.status[i])==='undefined'){
			  		deal.status[i] = {
				  	'date': currDate,
				  	'location': currLoc,
				  	'stage': i
				 } 
			  		
			  	} else{
			  		deal.status[i] = deal.status[i];
			  	}
			  	
			  }
		  }
		  console.log(deal.status);
		  
		  if(newStageId != oldStageId){
		  	this.stagesCollection.doc('stage-'+newStageId + '/deals/'+dealId).set(deal);
		  	this.stagesCollection.doc('stage-'+oldStageId + '/deals/'+dealId).delete();
		  }
		  
		});		
	}

	deleteStage(stage: Stage): void  {
	  this.stagesCollection.doc('stage-'+ stage.index).delete();
	}

	addDeal(deal: Deal): void  {
	  const id = this.afs.createId();
	  deal.id = id;
	  this.stagesCollection.doc('stage-'+ deal.stage).collection('deals').doc(id).set(Object.assign({}, deal));
	}

	updateDeal(deal: Deal): void{
		 this.stagesCollection.doc('stage-'+ deal.stage + '/deals/' + deal.id).set(deal);
	}


}