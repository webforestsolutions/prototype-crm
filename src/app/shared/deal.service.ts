import { Injectable, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import {Deal} from './deal';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()




export class DealService{

	dealsCollection: AngularFirestoreCollection<Deal>;
	deals: Observable<Deal[]>;
	deal: Observable<Deal>;

	constructor(private afs: AngularFirestore){
		this.dealsCollection = this.afs.collection('deals');
	}

	getDealsList(){
		this.deals = this.dealsCollection.valueChanges();
		
		return this.deals;
	}

	addDeal(deal: Deal): void  {
	  const id = this.afs.createId();

	  this.dealsCollection.doc('s' + deal.stage ).set(Object.assign({}, deal));
	 }



}