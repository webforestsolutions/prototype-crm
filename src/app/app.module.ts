import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgDatepickerModule } from 'ng2-datepicker';


import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';

export const firebaseConfig = environment.firebaseConfig;
import {AngularFirestoreModule} from 'angularfire2/firestore';

import { AppComponent } from './app.component';
import {DndModule} from 'ng2-dnd';

import { DealService } from './shared/deal.service';
import { StageService } from './shared/stage.service';

import { DragulaService, DragulaModule } from 'ng2-dragula/ng2-dragula';
import { QuillEditorModule } from 'ngx-quill-editor';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgDatepickerModule,
    DndModule.forRoot(),
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig), 
    AngularFirestoreModule,
    DragulaModule,
    QuillEditorModule
  ],
  providers: [DealService, StageService, DragulaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
